# DesQ Docs
## A simple Document Viewer for the DesQ Desktop Environment

This is a app to view various documents like pdf, epub, djvu etc. It is built to run smoothly on devices with different form factors.

### Dependencies (Debian package name [version] in Sid):
* Qt5 (qtbase5-dev [5.15.2+dfsg-2], qtbase5-dev-tools [5.15.2+dfsg-2])
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)
* qdocumentview (https://gitlab.com/marcusbritanicus/qdocumentview)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQDocs.git DesQDocs`
- Enter the `DesQDocs` folder
  * `cd DesQDocs`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Upcoming
* Support for epub, ps, eps, cbr, cbz, etc.
* Any other feature you request for... :)

### Known Bugs
* None of the QDocumentView objects created are deleted. This may cause memory leaks.
