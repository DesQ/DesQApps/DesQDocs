/**
 * This file is a part of DesQDocs.
 * DesQDocs is the default document viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtPrintSupport>

#include <desq/Utils.hpp>
#include <DFSettings.hpp>

// Local Headers
#include "Global.hpp"
#include "DesQDocs.hpp"
#include "DocsView.hpp"
#include <QDocumentNavigation.hpp>

DesQ::Docs::UI::UI( QWidget *parent ) : DesQUI::MainWindow( "Docs", parent ) {
    createUI();
    setWindowProperties();

    createShortcuts();

    if ( stack->count() ) {
        views.at( docsView->currentItem() )->setFocus();
    }
}


DesQ::Docs::UI::~UI() {
    delete docsView;
    delete stack;

    qDeleteAll( views );
}


void DesQ::Docs::UI::show() {
    bool maximized = docsSett->value( "Session::ShowMaximized" );

    if ( maximized ) {
        QMainWindow::showMaximized();
    }

    else {
        QMainWindow::showNormal();
    }

    qApp->processEvents();
}


void DesQ::Docs::UI::createUI() {
    docsView = new DocsView( this );
    docsView->hide();

    /* Build the SideBar */
    buildActions();

    stack = new QStackedWidget( this );
    setMainView( stack );

    /** Switch document */
    connect( docsView, &DesQ::Docs::DocsView::switchDocument, stack, &QStackedWidget::setCurrentIndex );
    connect( docsView, &DesQ::Docs::DocsView::closeDocument,  this,  &DesQ::Docs::UI::closeDocumentAt );
}


void DesQ::Docs::UI::setWindowProperties() {
    setAppTitle( "DesQ Docs" );
    setAppIcon( QIcon( ":/icons/desq-docs.png" ) );

    setGeometry( docsSett->value( "Session::Geometry" ) );
    setMinimumSize( 640, 480 );
}


void DesQ::Docs::UI::buildActions() {
    createActionBar( Qt::Vertical );

    // Page #0
    addAction(
        0,
        "Open", QIcon::fromTheme( "document-open" ),
        "open", "Open a document for viewing",
        false, false
    );

    addSeparator( 0 );

    addAction(
        0,
        "Reload", QIcon::fromTheme( "view-refresh" ),
        "reload", "Reload the current document",
        false, false
    );

    addSeparator( 0 );

    mZoomModeAction = addAction(
        0,
        "Zoom Mode", QIcon::fromTheme( "zoom-fit-best" ),
        "zoom-mode", "Change the zoom mode",
        false, false
    );

    mPageLayoutAction = addAction(
        0,
        "Page Layout", QIcon::fromTheme( "view-pages-single" ),
        "switch-layout", "Switch layout",
        false, false
    );

    addSeparator( 0 );

    addAction(
        0,
        "Rotate Left", QIcon::fromTheme( "object-rotate-left" ),
        "rotate-left", "Rotate the pages counter-clowckwise",
        false, false
    );

    addAction(
        0,
        "Rotate Right", QIcon::fromTheme( "object-rotate-right" ),
        "rotate-right", "Rotate the pages clowckwise",
        false, false
    );

    addSeparator( 0 );

    addAction(
        0,
        "View Mode", QIcon::fromTheme( "view-pages-continuous" ),
        "view-mode", "Show pages continuously",
        true, true
    );

    addSeparator( 0 );

    addAction(
        0,
        "Print", QIcon::fromTheme( "document-print" ),
        "print", "Print the current document",
        false, false
    );

    addSeparator( 0 );

    addAction(
        0,
        "Save as Image", QIcon::fromTheme( "image-x-generic" ),
        "save-image", "Save the current page as image",
        false, false
    );

    addSpace( 0, false );

    addAction(
        0,
        "Open documents", QIcon::fromTheme( "document-multiple" ),
        "show-docs", "Show open documents",
        false, false
    );

    connect( this, &DesQUI::MainWindow::actionRequested, this, &DesQ::Docs::UI::receiveMessage );

    if ( docsView->currentItem() < 0 ) {
        setActionDisabled( 0, 1 );
        setActionDisabled( 0, 2 );
        setActionDisabled( 0, 3 );
        setActionDisabled( 0, 4 );
        setActionDisabled( 0, 5 );
        setActionDisabled( 0, 6 );
        setActionDisabled( 0, 7 );
        setActionDisabled( 0, 8 );
        setActionDisabled( 0, 9 );
    }

    switchToPage( 0 );
}


void DesQ::Docs::UI::createShortcuts() {
    QAction *openAct = new QAction(
        QIcon::fromTheme( "document-open" ),
        "Open a Document",
        this
    );

    openAct->setShortcut( QKeySequence::Open );

    connect(
        openAct, &QAction::triggered, [ = ]() {
            receiveMessage( "open" );
        }
    );

    QAction *nextAct = new QAction(
        QIcon::fromTheme( "go-next-view" ),
        "Switch to next tab",
        this
    );

    nextAct->setShortcuts( { QKeySequence( Qt::CTRL | Qt::Key_Tab ), QKeySequence( Qt::CTRL | Qt::Key_PageDown ) } );

    connect(
        nextAct, &QAction::triggered, [ = ]() {
            int currentView = docsView->currentItem();

            if ( currentView == views.count() - 1 ) {
                currentView = 0;
            }

            else {
                currentView++;
            }

            stack->setCurrentIndex( currentView );
            docsView->setCurrentItem( currentView );
        }
    );

    QAction *prevAct = new QAction(
        QIcon::fromTheme( "go-previous-view" ),
        "Switch to previous tab",
        this
    );

    prevAct->setShortcuts( { QKeySequence( Qt::SHIFT | Qt::CTRL | Qt::Key_Tab ), QKeySequence( Qt::CTRL | Qt::Key_PageUp ) } );

    connect(
        prevAct, &QAction::triggered, [ = ]() {
            int currentView = docsView->currentItem();

            if ( currentView == 0 ) {
                currentView = views.count() - 1;
            }

            else {
                currentView--;
            }

            stack->setCurrentIndex( currentView );
            docsView->setCurrentItem( currentView );
        }
    );

    QAction *tabsAct = new QAction(
        QIcon::fromTheme( "documents-multiple" ),
        "Toggle the tabs view",
        this
    );

    tabsAct->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_T ) );

    connect(
        tabsAct, &QAction::triggered, [ = ]() {
            receiveMessage( "show-docs" );
        }
    );

    QAction *closeAct = new QAction(
        QIcon::fromTheme( "document-close" ),
        "Close the current document",
        this
    );

    closeAct->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_W ) );
    connect( closeAct, &QAction::triggered, this, &DesQ::Docs::UI::closeCurrentDocument );

    QAction *searchAct = new QAction(
        QIcon::fromTheme( "edit-find" ),
        "Search the current document",
        this
    );

    searchAct->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_F ) );

    connect(
        searchAct, &QAction::triggered, [ = ]() {
            if ( docsView->currentItem() < 0 ) {
                return;
            }

            QDocumentView *view = views.at( docsView->currentItem() );
            view->focusSearch();
        }
    );

    QAction *clearSearchAct = new QAction(
        QIcon::fromTheme( "edit-find" ),
        "Clear the search",
        this
    );

    clearSearchAct->setShortcut( QKeySequence( Qt::CTRL | Qt::SHIFT | Qt::Key_F ) );

    connect(
        clearSearchAct, &QAction::triggered, [ = ]() {
            if ( docsView->currentItem() < 0 ) {
                return;
            }

            QDocumentView *view = views.at( docsView->currentItem() );
            view->clearSearch();
        }
    );

    QAction *nextSearchAct = new QAction(
        QIcon::fromTheme( "edit-find" ),
        "Find the next instance of the search text",
        this
    );

    nextSearchAct->setShortcut( QKeySequence( Qt::Key_F3 ) );

    connect(
        nextSearchAct, &QAction::triggered, [ = ]() {
            if ( docsView->currentItem() < 0 ) {
                return;
            }

            QDocumentView *view = views.at( docsView->currentItem() );
            view->highlightNextSearchInstance();
        }
    );

    QAction *prevSearchAct = new QAction(
        QIcon::fromTheme( "edit-find" ),
        "Find the previous instance of the search text",
        this
    );

    prevSearchAct->setShortcut( QKeySequence( Qt::SHIFT | Qt::Key_F3 ) );

    connect(
        prevSearchAct, &QAction::triggered, [ = ]() {
            if ( docsView->currentItem() < 0 ) {
                return;
            }

            QDocumentView *view = views.at( docsView->currentItem() );
            view->highlightPreviousSearchInstance();
        }
    );

    QMainWindow::addAction( openAct );
    QMainWindow::addAction( nextAct );
    QMainWindow::addAction( prevAct );
    QMainWindow::addAction( tabsAct );
    QMainWindow::addAction( closeAct );
    QMainWindow::addAction( searchAct );
    QMainWindow::addAction( clearSearchAct );
    QMainWindow::addAction( nextSearchAct );
    QMainWindow::addAction( prevSearchAct );
}


void DesQ::Docs::UI::receiveMessage( QString action ) {
    if ( action == "open" ) {
        QStringList filenames = QFileDialog::getOpenFileNames(
            this,
            "DesQ Docs | Open a document for viewing",
            (docsView->currentItem() >= 0 ? views.at( docsView->currentItem() )->document()->filePath() : QDir::homePath() ),
            "Documents (*.pdf *.djv *.djvu);;All files (*.*)"
        );

        for ( QString file: filenames ) {
            createView( file );
        }
    }

    /* One or more specified files */
    else if ( action.startsWith( "open\n" ) ) {
        QStringList files = action.split( "\n", Qt::SkipEmptyParts );
        files.removeAt( 0 );                        // Remove "open"
        for ( QString file: files ) {
            createView( file );
        }
    }

    else if ( action == "reload" ) {
        if ( docsView->currentItem() && views.at( docsView->currentItem() )->document() ) {
            views.at( docsView->currentItem() )->document()->reload();
        }
    }

    else if ( action == "zoom-mode" ) {
        if ( views.count() && views.at( docsView->currentItem() )->document() ) {
            int newZMode = (views.at( docsView->currentItem() )->zoomMode() + 1) % 3;
            views.at( docsView->currentItem() )->setZoomMode( (QDocumentView::ZoomMode)newZMode );
            switch ( newZMode ) {
                case QDocumentView::ZoomMode::CustomZoom: {
                    setIcon( 0, mZoomModeAction, QIcon::fromTheme( "zoom-fit-best" ) );
                    break;
                }

                case QDocumentView::ZoomMode::FitToWidth: {
                    setIcon( 0, mZoomModeAction, QIcon::fromTheme( "zoom-fit-width" ) );
                    break;
                }

                case QDocumentView::ZoomMode::FitInView: {
                    setIcon( 0, mZoomModeAction, QIcon::fromTheme( "zoom-fit-page" ) );
                    break;
                }
            }
        }
    }

    else if ( action.startsWith( "rotate-" ) && views.count() ) {
        QDocumentRenderOptions rOpts = views.at( docsView->currentItem() )->renderOptions();

        int newRot = ( int )rOpts.rotation();

        if ( action == "rotate-left" ) {
            newRot--;

            if ( newRot < 0 ) {
                newRot = 3;
            }
        }

        else {
            newRot++;

            if ( newRot > 3 ) {
                newRot = 0;
            }
        }

        rOpts.setRotation( ( QDocumentRenderOptions::Rotation )newRot );
        views.at( docsView->currentItem() )->setRenderOptions( rOpts );
    }

    else if ( (action == "view-mode") && views.count() ) {
        views.at( docsView->currentItem() )->setLayoutContinuous( views.at( docsView->currentItem() )->isLayoutContinuous() == false );
    }

    else if ( action == "print" ) {
        prepareToPrint();
    }

    else if ( (action == "save-image") && views.count() ) {
        savePageAsImage( docsView->currentItem() );
    }

    else if ( (action == "switch-layout") && views.count() ) {
        QDocumentView *view = views.value( docsView->currentItem() );
        int           pgLyt = ( int )view->pageLayout();
        pgLyt = (pgLyt + 1) % 4;
        view->setPageLayout( (QDocumentView::PageLayout)pgLyt );

        switch ( pgLyt ) {
            case QDocumentView::SinglePage: {
                setIcon( 0, mPageLayoutAction, QIcon::fromTheme( "view-pages-single" ) );
                break;
            }

            case QDocumentView::FacingPages: {
                setIcon( 0, mPageLayoutAction, QIcon::fromTheme( "view-pages-facing" ) );
                break;
            }

            case QDocumentView::BookView: {
                setIcon( 0, mPageLayoutAction, QIcon::fromTheme( "view-pages-facing-first-centered.svg" ) );
                break;
            }

            case QDocumentView::OverView: {
                setIcon( 0, mPageLayoutAction, QIcon::fromTheme( "view-pages-overview" ) );
                break;
            }
        }
    }

    else if ( action == "show-docs" ) {
        if ( docsView->isVisible() ) {
            docsView->hide();

            if ( views.count() ) {
                views.value( docsView->currentItem() )->setFocus();
            }
        }

        else {
            docsView->show();

            if ( views.count() ) {
                docsView->setCurrentRow( docsView->currentItem() );
                docsView->setFocus();
            }
        }
    }
}


void DesQ::Docs::UI::createView( QString filename ) {
    int idx = docsView->addItem( filename );

    if ( idx < stack->count() ) {
        stack->setCurrentIndex( idx );
        return;
    }

    /** Create the document view */
    QDocumentView *view = new QDocumentView( this );

    view->setPageLayout( QDocumentView::SinglePage );
    view->setLayoutContinuous( true );

    /** Load the file */
    view->load( filename );
    views << view;

    /** Add the widget to the stack */
    stack->addWidget( view );
    stack->setCurrentIndex( idx );

    /** Focus the current view */
    view->setFocus();

    setActionEnabled( 0, 1 );
    setActionEnabled( 0, 2 );
    setActionEnabled( 0, 3 );
    setActionEnabled( 0, 4 );
    setActionEnabled( 0, 5 );
    setActionEnabled( 0, 6 );
    setActionEnabled( 0, 7 );
    setActionEnabled( 0, 8 );
    setActionEnabled( 0, 9 );
}


void DesQ::Docs::UI::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    if ( docsView ) {
        docsView->setFixedSize( QSize( 0.35 * width(), height() ) );
        docsView->move( 0.65 * width(), 0 );
    }
}


void DesQ::Docs::UI::keyReleaseEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        if ( docsView && docsView->isVisible() ) {
            docsView->hide();
            return;
        }
    }

    MainWindow::keyReleaseEvent( kEvent );
}


void DesQ::Docs::UI::closeEvent( QCloseEvent *cEvent ) {
    /** Store if the window was naximized */
    docsSett->setValue( "Session::ShowMaximized", isMaximized() );

    /** Save the geometry only if the window is not maximized */
    if ( isMaximized() == false ) {
        docsSett->setValue( "Session::Geometry", geometry() );
    }

    cEvent->accept();
}


void DesQ::Docs::UI::closeCurrentDocument() {
    if ( views.count() && (docsView->currentItem() >= 0) ) {
        closeDocumentAt( docsView->currentItem() );
    }
}


void DesQ::Docs::UI::closeDocumentAt( int idx ) {
    docsView->removeItem( idx );

    QDocumentView *doc = views.takeAt( idx );

    if ( doc ) {
        stack->removeWidget( doc );
        // Possibly a bug in QDocumentView. Deleting this causes a SIGSEGV.
        // delete doc;
    }

    /** First Document */
    if ( idx == 0 ) {
        if ( stack->count() ) {
            stack->setCurrentIndex( 0 );
            docsView->setCurrentItem( 0 );
        }
    }

    /** Last document */
    else if ( idx == stack->count() ) {
        if ( stack->count() ) {
            stack->setCurrentIndex( stack->count() - 1 );
            docsView->setCurrentItem( stack->count() - 1 );
        }
    }

    /** Somewhere in the middle: Load the next */
    else {
        stack->setCurrentIndex( idx );
        docsView->setCurrentItem( idx );
    }

    qDebug() << stack->count();

    if ( stack->count() == 0 ) {
        setActionDisabled( 0, 1 );
        setActionDisabled( 0, 2 );
        setActionDisabled( 0, 3 );
        setActionDisabled( 0, 4 );
        setActionDisabled( 0, 5 );
        setActionDisabled( 0, 6 );
        setActionDisabled( 0, 7 );
        setActionDisabled( 0, 8 );
        setActionDisabled( 0, 9 );
    }
}


void DesQ::Docs::UI::prepareToPrint() {
    // QPrinter *printer = new QPrinter();
    // printer->setPageOrientation( QPageLayout::Landscape );
    //
    // printer->setCollateCopies( true );
    // printer->setPageOrder( QPrinter::FirstPageFirst );
    //
    // printer->setColorMode( QPrinter::Color );
    // printer->setDuplex( QPrinter::DuplexAuto );
    //
    // QPrintDialog *prntDlg = new QPrintDialog( printer, this );
    //
    // if ( prntDlg->exec() == QDialog::Accepted ) {
    //     printer = prntDlg->printer();
    // }

    for ( QPrinterInfo pInfo: QPrinterInfo::availablePrinters() ) {
        if ( pInfo.printerName() == "HomeJet" ) {
            QPrinter *printer = new QPrinter( pInfo );
            qDebug() << "Printing to" << pInfo.printerName();
            views.at( 0 )->print( printer, QDocumentPrintOptions() );
        }
    }
}


void DesQ::Docs::UI::savePageAsImage( int curView ) {
    QDocumentNavigation *docNav = views.at( curView )->pageNavigation();
    int                 curPage = docNav->currentPage();

    QDocument *doc = views.at( curView )->document();
    QSizeF    pgSz = doc->pageSize( curPage );

    /* Obtain the page zoom factor */
    QInputDialog *zoomDlg = new QInputDialog( this );

    zoomDlg->setInputMode( QInputDialog::IntInput );
    zoomDlg->setLabelText(
        QString(
            "Size of page %1 is %2 x %3 px. Please set the zoom factor below if necesssary."
        ).arg( curPage + 1 ).arg( pgSz.width() ).arg( pgSz.height() )
    );
    zoomDlg->setIntRange( 10, 1600 );
    zoomDlg->setIntStep( 10 );
    zoomDlg->setIntValue( 100 );

    Q_FOREACH ( QSpinBox *sb, zoomDlg->findChildren<QSpinBox *>() ) {
        if ( sb ) {
            sb->setSuffix( " %" );
        }
    }

    if ( zoomDlg->exec() == QDialog::Accepted ) {
        /* Obtain the save file name */
        QStringList fmts;
        Q_FOREACH ( QByteArray fmtba, QImageWriter::supportedImageFormats() ) {
            QString fmt = QString::fromLatin1( fmtba );
            fmts << fmt.toUpper() + " Image (*." + fmt + ")";
        }

        QString *str = new QString( "PNG Image (*.png)" );
        QString fn   = QFileDialog::getSaveFileName(
            nullptr,
            "DesQDocs | Save Image",
            QDir::homePath() + "/untitled.png",
            fmts.join( ";;" ) + ";;All Files (*.*)", str
        );

        if ( fn.length() ) {
            // PageSaver *saver = new PageSaver( PdfDoc->pdfPath() + "/" + PdfDoc->pdfName(), pageNo,
            // zoomDlg->intValue(), fn );
            // saver->start();
            QImage img = doc->renderPage( curPage, zoomDlg->intValue() / 100.0, QDocumentRenderOptions() );
            img.save( fn );
        }
    }
}
