/**
 * This file is a part of DesQDocs.
 * DesQDocs is the default document viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

// DesQ Headers
#include <desqui/MainWindow.hpp>

// QDV Headers
#include <QDocument.hpp>
#include <QDocumentView.hpp>

namespace DesQ {
    namespace Docs {
        class UI;
        class DocsView;
    }
}

class DesQ::Docs::UI : public DesQUI::MainWindow {
    Q_OBJECT

    public:
        UI( QWidget *parent );
        ~UI();

        void show();

    private:
        /** Stacked widget */
        QStackedWidget *stack;

        /** Lists to store the views */
        QList<QDocumentView *> views;

        /** Widget to show the list of open document */
        DocsView *docsView;

        int mZoomModeAction;
        int mPageLayoutAction;

        void createUI();
        void setWindowProperties();

        void buildActions();
        void createShortcuts();

        void createView( QString );

        void closeCurrentDocument();
        void closeDocumentAt( int );

        void prepareToPrint();
        void savePageAsImage( int );

    protected:
        void resizeEvent( QResizeEvent * );
        void keyReleaseEvent( QKeyEvent * );

        void closeEvent( QCloseEvent * );

    public Q_SLOTS:
        void receiveMessage( QString );
};
