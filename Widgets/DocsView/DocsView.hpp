/**
 * This file is a part of DesQDocs.
 * DesQDocs is the default document viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

// DesQ Headers
#include <desqui/MainWindow.hpp>

// QDV Headers
#include <QDocument.hpp>
#include <QDocumentView.hpp>

namespace DesQ {
    namespace Docs {
        class DocsView;
        class DocItem;
    }
}

class DesQ::Docs::DocsView : public QListWidget {
    Q_OBJECT

    public:
        DocsView( QWidget *parent );

        int addItem( QString );
        void removeItem( int );

        int currentItem();
        void setCurrentItem( int );

        int documentIndex( QString );

    private:
        /** Lists to store the views */
        QStringList fileNames;
        int currentIndex = -1;

    protected:
        void keyReleaseEvent( QKeyEvent * );

    Q_SIGNALS:
        void switchDocument( int );
        void closeDocument( int );
};

class DesQ::Docs::DocItem : public QWidget {
    Q_OBJECT

    public:
        DocItem( QString, QWidget *parent = nullptr );

        QString documentPath();

    private:
        QString mPath;

    Q_SIGNALS:
        void closeDocument( QString );
};
