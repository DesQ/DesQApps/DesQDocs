/**
 * This file is a part of DesQDocs.
 * DesQDocs is the default document viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "DocsView.hpp"

DesQ::Docs::DocsView::DocsView( QWidget *parent ): QListWidget( parent ) {
    setSelectionMode( QAbstractItemView::SingleSelection );
    setSortingEnabled( false );

    setFrameStyle( QFrame::NoFrame );

    connect(
        this, &QListWidget::itemActivated, [ = ]( QListWidgetItem *item ) {
            QString key  = qobject_cast<DesQ::Docs::DocItem *>( itemWidget( item ) )->documentPath();
            currentIndex = fileNames.indexOf( key );
            hide();

            qDebug() << key << currentIndex;
            emit switchDocument( currentIndex );
        }
    );
}


int DesQ::Docs::DocsView::addItem( QString path ) {
    if ( not fileNames.contains( path ) ) {
        fileNames << path;

        QListWidgetItem *item = new QListWidgetItem( this );
        item->setSizeHint( QSize( width(), 48 ) );

        DocItem *docItem = new DocItem( path );
        connect(
            docItem, &DesQ::Docs::DocItem::closeDocument, [ = ]( QString path ) {
                emit closeDocument( fileNames.indexOf( path ) );
            }
        );

        QListWidget::addItem( item );
        setItemWidget( item, docItem );
    }

    currentIndex = fileNames.indexOf( path );
    emit switchDocument( currentIndex );

    return currentIndex;
}


void DesQ::Docs::DocsView::removeItem( int idx ) {
    if ( idx < 0 ) {
        return;
    }

    fileNames.removeAt( idx );

    DocItem *docItem = qobject_cast<DesQ::Docs::DocItem *>( itemWidget( item( idx ) ) );
    delete docItem;

    QListWidgetItem *itm = takeItem( idx );
    delete itm;
}


int DesQ::Docs::DocsView::currentItem() {
    return currentIndex;
}


void DesQ::Docs::DocsView::setCurrentItem( int idx ) {
    currentIndex = idx;
    setCurrentRow( idx );
}


int DesQ::Docs::DocsView::documentIndex( QString path ) {
    return fileNames.indexOf( path );
}


void DesQ::Docs::DocsView::keyReleaseEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        hide();
    }

    QListWidget::keyReleaseEvent( kEvent );
}


DesQ::Docs::DocItem::DocItem( QString path, QWidget *parent ) : QWidget( parent ) {
    mPath = path;

    QLabel      *icon     = new QLabel( this );
    QLabel      *docName  = new QLabel( this );
    QToolButton *closeBtn = new QToolButton( this );

    icon->setFixedSize( QSize( 36, 36 ) );

    if ( path.toLower().endsWith( "pdf" ) ) {
        icon->setPixmap( QIcon::fromTheme( "application-pdf" ).pixmap( 32 ) );
    }

    else if ( path.toLower().endsWith( "djv" ) ) {
        icon->setPixmap( QIcon::fromTheme( "image-vnd.djvu" ).pixmap( 32 ) );
    }

    else if ( path.toLower().endsWith( "djvu" ) ) {
        icon->setPixmap( QIcon::fromTheme( "image-vnd.djvu" ).pixmap( 32 ) );
    }

    else {
        icon->setPixmap( QIcon::fromTheme( "x-office-document" ).pixmap( 32 ) );
    }

    QFileInfo info( path );
    docName->setText( QString( "<b>%1</b><br><small>%2</small>" ).arg( info.fileName() ).arg( info.filePath() ) );

    closeBtn->setIcon( QIcon::fromTheme( "tab-close" ) );
    closeBtn->setIconSize( QSize( 24, 24 ) );
    closeBtn->setFixedSize( QSize( 36, 36 ) );
    closeBtn->setAutoRaise( true );
    connect(
        closeBtn, &QToolButton::clicked, [ = ] () {
            emit closeDocument( mPath );
        }
    );

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins( 6, 6, 6, 6 ) );

    lyt->addWidget( icon );
    lyt->addWidget( docName );
    lyt->addWidget( closeBtn );

    setLayout( lyt );
}


QString DesQ::Docs::DocItem::documentPath() {
    return mPath;
}
