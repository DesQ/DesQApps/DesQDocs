project(
    'DesQ Docs',
    'c',
	'cpp',
	version: '0.0.9',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_global_arguments( '-DPROJECT_VERSION="v@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

GlobalInc = include_directories( '.' )

# If use_qt_version is 'auto', we will first search for Qt6.
# If all Qt6 packages are available, we'll use it. Otherwise,
# we'll switch to Qt5.
if get_option('use_qt_version') == 'auto'
	# Check if Qt6 is available
	QtDeps = dependency(
		'qt6',
		modules: [ 'Core', 'Gui', 'Widgets', 'PrintSupport' ],
		required: false
	)

	DesQCore    = dependency( 'desq-core-qt6', required: false )
	DesQGui     = dependency( 'desq-gui-qt6', required: false )
	DesQWidgets = dependency( 'desq-widgets-qt6', required: false )

	DFApp       = dependency( 'df6application', required: false )
	DFSettings  = dependency( 'df6settings', required: false )
    DFUtils     = dependency( 'df6utils', required: false )
    DFXdg       = dependency( 'df6xdg', required: false )

    QDV         = dependency( 'Qt6DocumentView', required: false )

    Qt6_is_Found = QtDeps.found() and DesQCore.found() and DesQGui.found() and DesQWidgets.found() and DFApp.found()
    Qt6_is_Found = Qt6_is_Found and DFSettings.found() and DFUtils.found() and DFXdg.found() and QDV.found()

    if Qt6_is_Found
		Qt = import( 'qt6' )
		message( 'Using Qt6' )

	else
		Qt = import( 'qt5' )
		QtDeps = dependency(
			'qt5',
			modules: [ 'Core', 'Gui', 'Widgets', 'PrintSupport' ],
		)

    	DesQCore    = dependency( 'desq-core' )
    	DesQGui     = dependency( 'desq-gui' )
    	DesQWidgets = dependency( 'desq-widgets' )

    	DFApp       = dependency( 'df5application' )
    	DFSettings  = dependency( 'df5settings' )
        DFUtils     = dependency( 'df5utils' )
        DFXdg       = dependency( 'df5xdg' )

        QDV         = dependency( 'Qt5DocumentView' )
	endif

# User specifically wants to user Qt5
elif get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )

	QtDeps = dependency(
		'qt5',
		modules: [ 'Core', 'Gui', 'Widgets', 'PrintSupport' ],
		required: true
	)

	DesQCore    = dependency( 'desq-core' )
	DesQGui     = dependency( 'desq-gui' )
	DesQWidgets = dependency( 'desq-widgets' )

	DFApp       = dependency( 'df5application' )
	DFSettings  = dependency( 'df5settings' )
    DFUtils     = dependency( 'df5utils' )
    DFXdg       = dependency( 'df5xdg' )

    QDV         = dependency( 'Qt5DocumentView' )

# User specifically wants to user Qt6
elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )

	QtDeps = dependency(
		'qt6',
		modules: [ 'Core', 'Gui', 'Widgets','PrintSupport' ],
	)

	DesQCore    = dependency( 'desq-core-qt6', required: false )
	DesQGui     = dependency( 'desq-gui-qt6', required: false )
	DesQWidgets = dependency( 'desq-widgets-qt6', required: false )

	DFApp       = dependency( 'df6application', required: false )
	DFSettings  = dependency( 'df6settings', required: false )
    DFUtils     = dependency( 'df6utils', required: false )
    DFXdg       = dependency( 'df6xdg', required: false )

    QDV         = dependency( 'Qt6DocumentView', required: false )
endif


Deps = [ QtDeps, DesQCore, DesQGui, DesQWidgets, QDV, DFSettings, DFApp, DFUtils, DFXdg ]

Headers = [
    'UI/DesQDocs.hpp',
    'Widgets/DocsView/DocsView.hpp',
]

Sources = [
	'Main.cpp',
    'UI/DesQDocs.cpp',
    'Widgets/DocsView/DocsView.cpp',
]

Mocs = Qt.compile_moc(
	headers : Headers,
	dependencies: QtDeps
)

Resources = Qt.compile_resources(
	name: 'docs_rcc',
	sources: 'icons/icons.qrc'
)

desqdd = executable(
    'desq-docs', [ Sources, Mocs, Resources ],
    dependencies: Deps,
	include_directories: [ 'UI', 'Widgets', 'Widgets/DocsView' ],
    install: true
)

install_data(
	'desq-docs.desktop',
	install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'applications' )
)

# install_data(
	# 'Docs.conf',
	# install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'desq', 'configs' )
# )

install_data(
    'README.md', 'Changelog', 'ReleaseNotes',
    install_dir: join_paths( get_option( 'datadir' ), 'desq-docs' ),
)

install_data(
    'icons/desq-docs.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)

install_data(
    'icons/desq-docs.png',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', '256x256', 'apps' ),
)

summary = [
	'',
	'---------------',
	'DesQ Docs @0@'.format( meson.project_version() ),
	'---------------',
	''
]
message( '\n'.join( summary ) )
