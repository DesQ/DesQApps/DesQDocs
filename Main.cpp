/**
 * This file is a part of DesQDocs.
 * DesQDocs is the default document viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <signal.h>

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "Global.hpp"
#include "DesQDocs.hpp"

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFApplication.hpp>

#include <desq/Utils.hpp>

DFL::Settings *docsSett;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Docs.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Docs started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application app( argc, argv );

    // Set application info
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Docs" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-docs" );

    app.interceptSignal( SIGTERM, nullptr );

    if ( app.lockApplication() ) {
        docsSett = DesQ::Utils::initializeDesQSettings( "Docs", "Docs" );

        DesQ::Docs::UI *docs = new DesQ::Docs::UI( nullptr );
        QObject::connect( &app, &DFL::Application::messageFromClient, docs, &DesQ::Docs::UI::receiveMessage );
        docs->show();

        qApp->processEvents();

        /* Open files specified on CLI */
        if ( argc > 1 ) {
            QString cmd = "open";
            for ( int i = 1; i < argc; i++ ) {
                if ( app.arguments().at( i ).startsWith( "/" ) ) {
                    cmd += QString( "\n%1" ).arg( app.arguments().at( i ) );
                }

                else {
                    cmd += QString( "\n%1" ).arg( QDir::current().filePath( app.arguments().at( i ) ) );
                }
            }

            docs->receiveMessage( cmd );
        }
    }

    else {
        if ( argc > 1 ) {
            QString cmd = "open";
            for ( int i = 1; i < argc; i++ ) {
                if ( app.arguments().at( i ).startsWith( "/" ) ) {
                    cmd += QString( "\n%1" ).arg( app.arguments().at( i ) );
                }

                else {
                    cmd += QString( "\n%1" ).arg( QDir::current().filePath( app.arguments().at( i ) ) );
                }
            }

            app.messageServer( cmd );

            return 0;
        }
    }

    return app.exec();
}
